import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from '@react-navigation/native';
import Product from "../screens/Product/Product";
import DetalleProducto from "../Screens/DetailProduct";


const Stack = createStackNavigator();


export default ProductStack = () => {
    return (
        <Stack.Navigator>
          <Stack.Screen
            name="producto"
            component={Product}
            options={{ headerShown: false }}
          />
          <Stack.Screen 
            name="DetalleProducto" 
            component={DetalleProducto}
            options={{ headerShown: false }}
          />
        </Stack.Navigator>
    
        
      );
}





























































