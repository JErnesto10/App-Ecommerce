export const API_URL = "https://ecommerce.kowi.com.mx";
export const API_URL_ECOMMERCE = "https://appkowi-test.kowi.com.mx";
export const API_URL_IMAGES ="https://apprutas.kowi.com.mx/images/products/";
export const TOKEN = "token";
export const IDUSER = "iduser";
export const SEARCH_HISTORY = "search-history";
