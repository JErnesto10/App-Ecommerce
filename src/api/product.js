import { API_URL_ECOMMERCE } from "../utils/constants";

  export async function agregarProducto(formData) {
    try {
      //console.log("SRVICE:"+JSON.stringify(formData))
      const url = `${API_URL_ECOMMERCE}/api/addProduct`;
      const params = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      };
      const response = await fetch(url, params);
      const result = await response;
      return result;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
  export async function updateCarrito(formData) {
    
    try {
      console.log(formData);
      const url = `${API_URL_ECOMMERCE}/api/updateCarrito`;
      const params = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      };
      const response = await fetch(url, params);
      const result = await response.json();
      return result;
    } catch (error) {
      console.log(formData);
      return null;
    }
  }