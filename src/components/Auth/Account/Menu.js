import React from "react";
import { Alert } from "react-native";
import { List } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";
import useAuth from "../../../hooks/useAuth";

export default function Menu() {
  const navigation = useNavigation();
  const { logout } = useAuth();

  const logoutAccount = () => {
    Alert.alert(
      "Cerrar sesión",
      "¿Estas seguro de que quieres salir de tu cuenta?",
      [
        {
          text: "NO",
        },
        { text: "SI", onPress: logout },
      ],
      { cancelable: false }
    );
  };

  return (
    <>
      <List.Section>
        <List.Subheader style={{color:"#ff6900", fontSize:17 , marginTop:10}}>Mi cuenta</List.Subheader>
        <List.Item
          title="Cambiar nombre"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Cambia el nombre de tu cuenta"
          left={(props) => <List.Icon {...props} icon="face" />}
          
        />
        
        <List.Item
          title="Cambiar contraseña"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Cambia el contraseña de tu cuenta"
          left={(props) => <List.Icon {...props} icon="key" />}
          
        />
        <List.Item
          title="Mis direcciones"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Administra tus direcciones de envio"
          left={(props) => <List.Icon {...props} icon="map" />}
          
        />
        
      
      </List.Section>

      <List.Section>
        <List.Subheader style={{color:"#ff6900", fontSize:17 , marginTop:10}}>Aplicación</List.Subheader>
        <List.Item 
          title="Pedidos"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Listado de todos los pedidos"
          left={(props) => <List.Icon {...props} icon="clipboard-list" />}
          
        />

        <List.Item
          title="Reportar Error"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Reporta cualquier error que se te pueda presentar"
          left={(props) => <List.Icon {...props} icon="alert" />}
          
        />

        <List.Item
          title="Cerrar sesión"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Cierra esta sesion y inicia con otra"
          left={(props) => <List.Icon {...props} icon="logout" />}
          onPress={logoutAccount}
        />
      </List.Section>
    </>
  );
}
