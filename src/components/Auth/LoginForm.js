import React, { useState } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { TextInput, Button,Title } from "react-native-paper";
import { useFormik } from "formik";
import * as Yup from "yup";
import Toast from "react-native-root-toast";
import useAuth from "../../hooks/useAuth";
import { loginApi } from "../../api/user";
import { StatusBar } from "expo-status-bar";

export default function LoginForm(props) {
  const { changeForm } = props;
  const [loading, setLoading] = useState(false);
  const { login } = useAuth();

  
  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),

    onSubmit: async (formData) => {
      setLoading(true);
      try {
        const response = await loginApi(formData);
        if (response.statusCode) throw response.respuesta;
        login(response);
        console.log(response);
      } catch (error) {
        console.log(error);
        Toast.show(error, {
          position: Toast.positions.CENTER,
        });
        setLoading(false);
      }
    },
  });


  return (
    <View style={{ backgroundColor: "white", height: "100%"}}>
      <StatusBar style="auto"/>
      <Image
        source={require("../../../assets/ecomerceLogo.png")}
        style={{ width: "100%", height: "55%" }}
      />
      <Title style={{ fontSize: 18, alignSelf: "center" }}>Iniciar sesíon</Title>
      <Text style={{ fontSize: 14, alignSelf: "center", marginTop: 5 }}>
        Ingresa tus datos
      </Text>
      <TextInput
        
        style={{
          marginTop: 5,
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
        }}
        mode="outlined"
        label="Usuario"
        placeholder="Ingrese usuario"
        theme={{  colors: {primary: 'orange'},roundness: 30 }} 
        left={<TextInput.Icon name="account-circle" color={"#e5893c"} />}
        onChangeText={(text) => formik.setFieldValue("username", text)}
        value={formik.values.username}
        error={formik.errors.username}
        
      />
      <TextInput
        style={{
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
        }}
        mode="outlined"
        label="Contraseña"
        theme={{  colors: {primary: 'orange'},roundness: 30 }} 
        placeholder="Ingrese contraseña"
        left={<TextInput.Icon name="lock" color={"#e5893c"} />}
        onChangeText={(text) => formik.setFieldValue("password", text)}
        value={formik.values.password}
        error={formik.errors.password}
        secureTextEntry
      />
      <Button
        style={{
          marginTop: 10,
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          borderRadius: 15,
          borderWidth: 1,
          backgroundColor: "#ff6900",
        }}
        icon="import"
        mode="contained"
        uppercase={false}
        onPress={formik.handleSubmit}
        loading={loading}>
        <Text style={{ color: "white" }}>Iniciar sesión</Text>
      </Button>
      <Button
        style={{
          marginTop: 5,
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          borderRadius: 15,
          borderWidth: 1,
        }}
        icon={"file-account-outline"}
        mode="outlined"
        color="#ff6900"
        uppercase={false}
        onPress={changeForm}
      >
        Registrarse
      </Button>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "flex-end",
          marginTop: 25,
          marginRight: 10,
        }}
      >
      </View>
    </View>
  );
}

function initialValues() {
  return {
    username: "",
    password: "",
  };
}

function validationSchema() {
  return {
    username: Yup.string().required(true),
    password: Yup.string().required(true),
  };
}

const styles = StyleSheet.create({
  
});
