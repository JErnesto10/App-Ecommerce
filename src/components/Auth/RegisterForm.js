import React,{useState} from 'react'
import { View, Text,ScrollView,SafeAreaView,StyleSheet } from 'react-native'
import { TextInput, Button,Title,IconButton } from "react-native-paper";
import { Header } from 'react-native-elements';
import { registerUser } from "../../api/user";
import { useFormik } from "formik";
import * as Yup from "yup";

export default function RegisterForm(props) {
  const {changeForm} = props;
  const [loading, setLoading] = useState(false);


  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      
      try {
        setLoading(true);
        const response = await registerUser(formData);
        console.log("STATUS:"+response)
        if(response=="200"){
          alert("Usuario registrado con éxito");
          changeForm();
        }else{
          alert("El usuario ya existe, favor de validar sus datos");
        }
       
      } catch (error) {
        setLoading(false);
      }
    },
  });

  function initialValues() {
    return {
      name: "",
      username: "",
      email: "",
      phone: "",
      city: "",
      codigopostal: "",
      colonia: "",
      address: "",
      numeroexterior: "",
      password: "",
    };
  }

  function validationSchema() {
    return {
      name: Yup.string().required(true)
    };
  }

    return (
      <SafeAreaView>
      <ScrollView>
        
      <View style={{ backgroundColor: "white", height: "100%" }}>
      
      <Text style={{ fontSize: 16, alignSelf: "center", marginTop: 5 }}>
        Registra tus datos
      </Text>
      <TextInput
        onChangeText={(text) => formik.setFieldValue("name", text)}
        value={formik.values.name}
        error={formik.errors.name}
        style={{
          marginTop: 5,
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
          
        }}
        mode="outlined"
        label="Nombre"
        theme={{  colors: {primary: 'orange'},roundness: 30 }}
        placeholder="Ingrese nombre"
        left={<TextInput.Icon name="account-circle" color={"#e5893c"} />}
      />
      <TextInput
      onChangeText={(text) => formik.setFieldValue("username", text)}
      value={formik.values.username}
      error={formik.errors.username}
        style={{
          marginTop: 5,
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
          
        }}
        mode="outlined"
        label="Usuario"
        theme={{  colors: {primary: 'orange'},roundness: 30 }}
        placeholder="Ingrese usuario"
        left={<TextInput.Icon name="fingerprint" color={"#e5893c"} />}
      />
      <TextInput
      onChangeText={(text) => formik.setFieldValue("email", text)}
      value={formik.values.email}
      error={formik.errors.email}
        style={{
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
          
        }}
        mode="outlined"
        label="Correo"
        theme={{  colors: {primary: 'orange'},roundness: 30 }}
        placeholder="Ingrese correo"
        left={<TextInput.Icon name="email" color={"#e5893c"} />}
      />
      <TextInput
      onChangeText={(text) => formik.setFieldValue("password", text)}
      value={formik.values.password}
      error={formik.errors.password}
        style={{
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
          
        }}
        mode="outlined"
        label="Contraseña"
        theme={{  colors: {primary: 'orange'},roundness: 30 }}
        placeholder="Ingrese contraseña"
        secureTextEntry={true}
        left={<TextInput.Icon name="lock" color={"#e5893c"} />}
      />
      <TextInput
      onChangeText={(text) => formik.setFieldValue("phone", text)}
      value={formik.values.phone}
      error={formik.errors.phone}
        style={{
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
          
        }}
        mode="outlined"
        label="Teléfono"
        theme={{  colors: {primary: 'orange'},roundness: 30 }}
        placeholder="Ingrese teléfono"
        left={<TextInput.Icon name="phone" color={"#e5893c"} />}
      />
      <TextInput
      onChangeText={(text) => formik.setFieldValue("city", text)}
      value={formik.values.city}
      error={formik.errors.city}
        style={{
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
          
        }}
        mode="outlined"
        label="Ciudad"
        theme={{  colors: {primary: 'orange'},roundness: 30 }}
        placeholder="Seleccione ciudad"
        left={<TextInput.Icon name="city" color={"#e5893c"} />}
      />
      <TextInput
      onChangeText={(text) => formik.setFieldValue("codigopostal", text)}
      value={formik.values.codigopostal}
      error={formik.errors.codigopostal}
        style={{
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
          
        }}
        mode="outlined"
        label="Código postal"
        theme={{  colors: {primary: 'orange'},roundness: 30 }}
        placeholder="Ingrese código postal"
        left={<TextInput.Icon name="email-open-multiple" color={"#e5893c"} />}
      />
      <TextInput
      onChangeText={(text) => formik.setFieldValue("address", text)}
      value={formik.values.address}
      error={formik.errors.address}
        style={{
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
          
        }}
        mode="outlined"
        label="Dirección"
        theme={{  colors: {primary: 'orange'},roundness: 30 }}
        placeholder="Ingrese Dirección"
        left={<TextInput.Icon name="map-marker" color={"#e5893c"} />}
      />
      <TextInput
      onChangeText={(text) => formik.setFieldValue("colonia", text)}
      value={formik.values.colonia}
      error={formik.errors.colonia}
        style={{
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
          
        }}
        mode="outlined"
        label="Colonia"
        theme={{  colors: {primary: 'orange'},roundness: 30 }}
        placeholder="Ingrese colonia"
        left={<TextInput.Icon name="text-box-outline" color={"#e5893c"} />}
      />
      <TextInput
      onChangeText={(text) => formik.setFieldValue("numeroexterior", text)}
      value={formik.values.numeroexterior}
      error={formik.errors.numeroexterior}
        style={{
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          height: 50,
          
        }}
        mode="outlined"
        label="Número exterior"
        theme={{  colors: {primary: 'orange'},roundness: 30 }}
        placeholder="Ingrese numero exterior"
        left={<TextInput.Icon name="text-box-outline" color={"#e5893c"} />}
      />
      <Button
        style={{
          marginTop: 10,
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          borderRadius: 15,
          borderWidth: 1,
          backgroundColor:'#ff6900'
        }}
        icon="import"
        mode="contained"
        uppercase={false}
        onPress={
          formik.handleSubmit
        }
      >
        <Text style={{ color: "white" }}>Registrar</Text>
      </Button>
      <Button
        style={{
          marginTop: 5,
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          borderRadius: 15,
          borderWidth: 1,
        }}
        icon={"file-account-outline"}
        mode="outlined"
        color="#ff6900"
        uppercase={false}
        onPress={changeForm}
      >
        Iniciar sesion
      </Button>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "flex-end",
          marginTop: 25,
          marginRight: 10,
        }}
      >
        <Text style={{ color: "#e5893c", fontSize: 12 }}>eCommerce v1.0</Text>
      </View>
    </View>    
    </ScrollView>
    </SafeAreaView>
  );
  
}

const styles = StyleSheet.create({
 
  heading: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
  },
 
});
