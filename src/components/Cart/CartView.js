import React, { useState,useEffect,useMemo } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import CartService from "./CartService";
import { useIsFocused } from '@react-navigation/native';


export default function CartView() {
    return (
        <View >
            <CartService />
        </View>
    )
}


const styles = StyleSheet.create({

    container: {
        padding: 10,
        marginTop: 20,
    },

    title: {
        fontWeight: "bold",
        fontSize: 20,
        marginBottom: 10,
    },

});