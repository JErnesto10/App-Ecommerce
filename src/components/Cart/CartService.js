import React, {useState,useEffect} from 'react'
import { View, 
    Text,
     ScrollView, 
     StyleSheet,
     TextInput, 
     Platform,
     TouchableOpacity,
     Picker,
    TouchableWithoutFeedback,
    ToastAndroid } from 'react-native'
import { useFormik } from "formik";
import { map } from "lodash";
import { RadioButton } from "react-native-paper";
import * as Yup from "yup";
import Toast from "react-native-root-toast";
import { useNavigation } from "@react-navigation/native";
import DatePicker from "react-native-datepicker";
import { updateCarrito } from "../../api/product";



export default function CartService() {

  const [idUsuario, setIdUsuario] = useState(null);
  const navigation = useNavigation();
  const [date, setDate] = useState(new Date());
  const [loading, setLoading] = useState(false);
  const [selectedValue, setSelectedValue] = useState("20");
  const lstCarrito=[{"id_cart_details":1,"name_short":"TOCINO","precio_producto":25,"quantity":"3","iva":0,"importe":10}]

  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      setLoading(true);
      try {
        const response = await updateCarrito(formData);        
        if (response) { 
              Platform.OS === 'android'?ToastAndroid.show("Pedido realizado con exito", ToastAndroid.SHORT)
              :Toast.show("Pedido realizado con exito", { position: Toast.positions.CENTER, });
        }else{
          console.log("RESPONSE ERROR"+response)
        }    
      } catch (error) {
        console.log("ERROR");
        setLoading(false);    
      }
    },
  });

    return (
      <ScrollView style={{ width: "100%" }}>
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
           ,
          }}
        >
          {/* Titulo principal */}
          <Text
            style={{
              fontSize: 25,
              alignSelf: "center",
              color: "#ff6900",
              fontWeight: "700",
              marginTop: 10,
            }}
          >
            Carrito de compras
          </Text>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginHorizontal: 15,
              marginTop: 15,
              paddingHorizontal: 10,
              paddingVertical: 2,
              backgroundColor: "#fff",
              borderBottomWidth: 1,
              borderBottomColor: "#D8D8D8",
            }}
          >
            {/* TH */}
            <Text style={{ width: "16%", fontSize: 10, color: "black" }}>
              Nombre
            </Text>
            <Text style={{ width: "16%", fontSize: 10, color: "black" }}>
              Precio
            </Text>
            <Text style={{ width: "16%", fontSize: 10, color: "black" }}>
              Cantidad
            </Text>
            <Text style={{ width: "16%", fontSize: 10, color: "black" }}>
              Iva
            </Text>
            <Text style={{ width: "16%", fontSize: 10, color: "black" }}>
              Subtotal
            </Text>
            <Text style={{ width: "16%", fontSize: 10, color: "black" }}>
              Eliminar
            </Text>
          </View>

          <View style={{ flex: 1, padding: 15, alignSelf: "center" }}>
            {map(lstCarrito, (item) => (
              <React.Fragment key={item.id_cart_details}>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 0,
                    paddingVertical: 2,
                    width: "100%",
                  }}
                >
                  
                  <Text
                    style={{
                      width: "16%",
                      height: 50,
                      fontSize: 10,
                      color: "#7E7E7E",
                      alignSelf: "center",
                    }}
                  >
                    {item.name_short}
                  </Text>
                  <Text
                    style={{
                      width: "16%",
                      height: 50,
                      fontSize: 10,
                      color: "#7E7E7E",
                      alignSelf: "center",
                    }}
                  >
                    ${item.precio_producto}
                  </Text>
                  <Text
                    style={{
                      width: "16%",
                      height: 50,
                      fontSize: 10,
                      color: "#7E7E7E",
                    }}
                  >
                    {item.quantity}KG
                  </Text>
                  <Text
                    style={{
                      width: "16%",
                      height: 50,
                      fontSize: 10,
                      color: "#7E7E7E",
                    }}
                  >
                    ${item.iva}
                  </Text>
                  <Text
                    style={{
                      width: "16%",
                      height: 50,
                      fontSize: 10,
                      color: "#7E7E7E",
                    }}
                  >
                    ${item.importe}
                  </Text>
                  <TouchableWithoutFeedback
                    onPress={() => navigation.navigate("DetalleProducto", {})}>
                    <Text
                      style={{
                        width: "16%",
                        height: 50,
                        fontSize: 15,
                        color: "red",
                        marginTop: -10,
                      }}
                    >
                      X
                    </Text>
                  </TouchableWithoutFeedback>
                </View>
              </React.Fragment>
            ))}

            <View
              style={{
                alignItems: "center",
                marginTop: 0,
                paddingVertical: 2,
                width: "100%",
                alignSelf: "center",
              }}
            >
              
              <View
                style={{
                  alignItems: "center",
                  marginTop: 0,
                  paddingVertical: 2,
                  width: "100%",
                  marginTop: 20,
                }}
              >
                <Text style={styles.label}>¿Requiere factura?</Text>
                <Text>{formik.values.factura}</Text>
                <RadioButton.Group
                  onValueChange={(text) =>
                    formik.setFieldValue("factura", text)
                  }
                  value={formik.values.factura}
                  error={formik.errors.factura}
                >
                  <View
                    style={{
                      alignItems: "center",
                      marginTop: 0,
                      width: "100%",
                      flexDirection: "row",
                    }}
                  >
                    <View style={{ width: "25%", flexDirection: "row" }}>
                      <RadioButton value="si"></RadioButton>
                      <Text style={{ fontSize: 24 }}>SI</Text>
                    </View>
                    <View style={{ width: "25%", flexDirection: "row" }}>
                      <RadioButton value="no"></RadioButton>
                      <Text style={{ fontSize: 24 }}>No</Text>
                    </View>
                  </View>
                </RadioButton.Group>
              </View>
              <Text
                style={{
                  fontSize: 12,
                  alignSelf: "center",
                  color: "#808080",
                  fontWeight: "700",
                  marginTop: 25,
                  paddingBottom: 0,
                }}
              >
                Importe a pagar (total):
              </Text>
              <Text
                style={{
                  fontSize: 40,
                  alignSelf: "center",
                  color: "#ff6900",
                  fontWeight: "700",
                  marginTop: 0,
                  paddingBottom: 25,
                }}
              >
                $150
              </Text>
              <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 30,
                    backgroundColor: "#fff",
                    paddingVertical: 0,
                    borderRadius: 23,
                    width: 300,
                  }}
                >
                  {/* Iniciar sesion */}
                  <TouchableOpacity
                    mode="contained"
                    onPress={formik.handleSubmit}
                    loading={setLoading}
                    disabled={false}
                    style={styles.buttonagregar2}
                  >
                    <Text
                      style={{
                        fontSize: 24,
                        color: "#fff",
                        alignSelf: "center",
                        paddingTop: 5,
                        fontWeight: "600",
                      }}
                    >
                      Realizar pedido
                    </Text>
                  </TouchableOpacity>
                </View>
            </View>
        </View>
      </View>
      </View>
    </ScrollView>
    )
}

function validationSchema() {
  return {
    id: Yup.string().required(true),
    idcentroventa: Yup.string().required(true),
    userid: Yup.string().required(true),
    city: Yup.string().required(true),
    contador: Yup.string().required(true),
    count_products: Yup.string().required(true),
    fechaE: Yup.string().required(true),
    direccionselect: Yup.string().required(true),
    factura: Yup.string().required(true),
    tokencode: Yup.string().required(true),
    organization_id: Yup.string().required(true),
  };
}

function initialValues() {
  return {
    id: "",
    idcentroventa: "",
    userid: "",
    city: "",
    contador: "",
    count_products: "",
    fechaE: "",
    direccionselect: "",
    factura: "",
    tokencode: "",
    organization_id: "",
  };
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "row",
      flexWrap: "wrap",
      alignItems: "flex-start",
      margin: -3,
      backgroundColor: "#fff",
    },
    containerProduct: {
      width: "100%",
      padding: 3,
    },
    product: {
      backgroundColor: "#f0f0f0",
      padding: 10,
    },
    image: {
      height: "100%",
      width: "50%",
      resizeMode: "contain",
    },
    name: {
      marginTop: 15,
      fontSize: 18,
    },
    buttonagregar: {
      backgroundColor: "#FF7709",
      borderRadius: 23,
      width: "100%",
      height: 40,
      marginBottom: 0,
    },
    buttonagregar2: {
      backgroundColor: "#ff6900",
      borderRadius: 23,
      width: "100%",
      height: 40,
      marginBottom: 0,
    },
    buttonagregar3: {
      backgroundColor: "#FF7709",
      borderRadius: 23,
      width: "50%",
      height: 70,
      marginBottom: 0,
    },
  });
  