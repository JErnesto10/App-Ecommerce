import React,  {useState,useEffect,useMemo}  from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ProductView from "./ProductViewTest";
import jwtDecode from 'jwt-decode';
import ProductContext from "../../context/ProductContext";
import { API_URL } from "../../utils/constants";

export default function ProductServiceTest() {
    const [carnesFrias, setCarnesFrias] = useState(null);
    const [cortesFinos, setCortesFinos] = useState(null);
    const [especialiades, setEspecialidades] = useState(null);
    const [lineaTuri, setLineaTuri] = useState(null);
    const [marinados, setMarinados] = useState(null);
    const [rostizados, setRostizados] = useState(null);
    const [idUser, setIdUser] = useState(undefined);
    const [idCart, setIdCart] = useState(null);
    

    useEffect(() => {
        (async () => {
          
          //Productos con categoria5=CARNES FRIAS  
          const url = `${API_URL}/api/productCategory/5`
          fetch(url).then((response) => response.json()).then((responseJson) => {
            let dataSource = [];
      
            Object.values(responseJson).forEach(item => {
                dataSource = dataSource.concat(item);
            });
            setCarnesFrias(dataSource);

        });
        //Productos con categoria6=CORTES FINOS 
        const url2 = `${API_URL}/api/productCategory/6`
          fetch(url2).then((response) => response.json()).then((responseJson) => {
            let dataSource = [];
      
            Object.values(responseJson).forEach(item => {
                dataSource = dataSource.concat(item);
            });
      
            setCortesFinos(dataSource);
        });

        //Productos con categoria7=ESPECIALIDADES
        const url3 = `${API_URL}/api/productCategory/7`
          fetch(url3).then((response) => response.json()).then((responseJson) => {
            let dataSource = [];
      
            Object.values(responseJson).forEach(item => {
                dataSource = dataSource.concat(item);
            });
      
            setEspecialidades(dataSource);
        });

        //Productos con categoria8=LINEA TURI
        const url4 = `${API_URL}/api/productCategory/8`
          fetch(url4).then((response) => response.json()).then((responseJson) => {
            let dataSource = [];
      
            Object.values(responseJson).forEach(item => {
                dataSource = dataSource.concat(item);
            });
      
            setLineaTuri(dataSource);
        });

        //Productos con categoria9=MARINADOS
        const url5 = `${API_URL}/api/productCategory/9`
          fetch(url5).then((response) => response.json()).then((responseJson) => {
            let dataSource = [];
      
            Object.values(responseJson).forEach(item => {
                dataSource = dataSource.concat(item);
            });
      
            setMarinados(dataSource);
        });

         //Productos con categoria10=ROSTIZADOS
         const url6 = `${API_URL}/api/productCategory/10`
         fetch(url6).then((response) => response.json()).then((responseJson) => {
           let dataSource = [];
     
           Object.values(responseJson).forEach(item => {
               dataSource = dataSource.concat(item);
           });
     
           setRostizados(dataSource);
       });

        
  
        })();
      }, []);

      const  authData = useMemo(
        () => ({
          idUser,
    
        }),
        [idUser]
      );
      

    return (
        <ProductContext.Provider value={authData}> 
        <View> 
            {carnesFrias && cortesFinos && especialiades && lineaTuri && marinados && rostizados &&
            <ProductView 
            carnesFrias={carnesFrias} 
            cortesFinos={cortesFinos} 
            especialiades={especialiades}
            lineaTuri={lineaTuri}
            marinados={marinados} 
            rostizados={rostizados} 
            idCart={idCart}/>}
        </View>
        </ProductContext.Provider> 
    )
}


const styles = StyleSheet.create({

    container: {
       padding: 10,
       marginTop: 20,
    },
  
    title:{
     fontWeight: "bold",
     fontSize: 20,
     marginBottom: 10,
    },
  
  });