import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableWithoutFeedback,
  } from "react-native";
  import { map } from 'lodash';
  import { useNavigation } from '@react-navigation/native';
  import { API_URL_IMAGES } from '../../utils/constants';

export default function ProductView(props) {

    const { products } = props;

    const navigation = useNavigation();

    const goToProduct = (idproduct) => {
        navigation.push("producto", { idproduct: idproduct });
      };


      return (
        <View style={styles.container}>
          {map(products, (product) => (
            <TouchableWithoutFeedback
              key={product.idproduct}
              onPress={() => goToProduct(product.idproduct)}
            >
              <View style={styles.containerProduct}>
                <View style={styles.product}>
                  <Image
                    style={styles.image}
                    source={{
                      uri: `${API_URL_IMAGES}${product.image}`,
                    }}
                  />
                  <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">
                    {product.nameproduct}
                  </Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
          ))}
        </View>
      );
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "row",
      flexWrap: "wrap",
      alignItems: "flex-start",
      margin: -3,
    },
    containerProduct: {
      width: "50%",
      padding: 3,
    },
    product: {
      backgroundColor: "#f0f0f0",
      padding: 10,
    },
    image: {
      height: 150,
      resizeMode: "contain",
    },
    name: {
      marginTop: 15,
      fontSize: 18,
    },
  });
  