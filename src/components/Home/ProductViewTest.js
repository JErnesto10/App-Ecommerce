import React, { useRef,useState } from 'react';
import {Text, View,SafeAreaView, TouchableOpacity,Image } from 'react-native';
import { API_URL_IMAGES } from '../../utils/constants';
import { useNavigation } from "@react-navigation/native";
import Carousel from 'react-native-snap-carousel';
import { Button,TextInput } from "react-native-paper";

export default function ProductViewTest(props) {

    const { carnesFrias } = props;
    const { cortesFinos } = props;
    const { especialiades } = props;
    const { lineaTuri } = props;
    const { marinados } = props;
    const { rostizados } = props;   
    const isCarrousel =useRef(null);

    function goToProduct(idproduct){
        //const navigation = useNavigation();
        //navigation.navigate("DetalleProducto", { idproduct: idproduct });
        alert(idproduct)
    }

    const _renderItem = ({item,index}) => {
        return (
          <View style={{
              backgroundColor:'#f1f1f1',
              borderRadius: 20,
              height: 250,
              padding: 25,
              marginLeft: 25,
              marginRight: 0,
              shadowColor: "black" }}>
            <Image  style={{ width: "100%", height: "70%"}}
                   source={{
                    uri: `${API_URL_IMAGES}${item.imagen}`,
                }}/>
            <Text style={{fontSize: 15,alignSelf:'center'}} >{item.producto}</Text>
            <Text style={{fontSize: 13, alignSelf:'center', color:'#ff6900'}}>$ {item.precio}</Text>
            <Button
                    style={{                   
                    width: "100%",                  
                    borderRadius: 15,
                    borderWidth: 1,
                    backgroundColor: "#ff6900",
                    }}
                    icon="import"
                    mode="contained"
                    uppercase={false}
                    onPress={() => goToProduct(item.producto)}
                >
                <Text style={{ color: "white" }}> Ver producto</Text>
            </Button>
          </View>

        )
    }

        return (
          <SafeAreaView style={{flex: 1, backgroundColor:'white',  }}>
             <View >
                <Image source={{uri: "https://appkowi-test.kowi.com.mx/images/APP-E-COMMERCE-12.png"}}
                    style={{width: "100%", height: 180, resizeMode: 'stretch'}} />
             </View>
              <View>
                    <TextInput
                        style={{
                        marginTop: 5,
                        width: "90%",
                        marginLeft: "10%",
                        marginRight: "10%",
                        height: 50,
                        borderRadius:25,
                        height: 40,
                        paddingBottom:10,
                        right:15
                        }}
                        mode="outlined"
                        label="Buscar"
                        placeholder="Buscar producto"
                        theme={{ colors: {primary: 'orange'},roundness: 30 }} 
                        left={<TextInput.Icon name="magnify" color={"#e5893c"}/>}
                    />
                </View>
            <Text style={{color:'black', fontSize:20, textAlign:'left', marginBottom:20, marginTop:10, marginLeft:10, textDecorationLine: 'underline'}}>Carnes frías </Text>
            <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center', paddingBottom:10 }}>
                <Carousel
                  layout={"default"}
                  ref={isCarrousel}
                  data={carnesFrias}
                  sliderWidth={300}
                  itemWidth={300}
                  renderItem={_renderItem} />
            </View>
            <Text style={{color:'black', fontSize:20, textAlign:'left', marginBottom:20, marginTop:10,marginLeft:10,textDecorationLine: 'underline'}}>Cortes finos </Text>
            <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center',  paddingBottom:10 }}>
                <Carousel
                  layout={"default"}
                  ref={isCarrousel}
                  data={cortesFinos}
                  sliderWidth={300}
                  itemWidth={300}
                  renderItem={_renderItem} />
            </View>
            <Text style={{color:'black', fontSize:20, textAlign:'left', marginBottom:20, marginTop:10,marginLeft:10,textDecorationLine: 'underline'}}>Especialidades </Text>
            <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center',  paddingBottom:10 }}>
                <Carousel
                  layout={"default"}
                  ref={isCarrousel}
                  data={especialiades}
                  sliderWidth={300}
                  itemWidth={300}
                  renderItem={_renderItem} />
            </View>
            <Text style={{color:'black', fontSize:20, textAlign:'left', marginBottom:20, marginTop:10,marginLeft:10,textDecorationLine: 'underline'}}>Linea Turi </Text>
            <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center',  paddingBottom:10 }}>
                <Carousel
                  layout={"default"}
                  ref={isCarrousel}
                  data={lineaTuri}
                  sliderWidth={300}
                  itemWidth={300}
                  renderItem={_renderItem} />
            </View>
            <Text style={{color:'black', fontSize:20, textAlign:'left', marginBottom:20, marginTop:10,marginLeft:10,textDecorationLine: 'underline'}}>Marinados,Adobados y Ahumados </Text>
            <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center',  paddingBottom:10 }}>
                <Carousel
                  layout={"default"}
                  ref={isCarrousel}
                  data={marinados}
                  sliderWidth={300}
                  itemWidth={300}
                  renderItem={_renderItem} />
            </View>
            <Text style={{color:'black', fontSize:20, textAlign:'left', marginBottom:20, marginTop:10,marginLeft:10,textDecorationLine: 'underline'}}>Rostizados </Text>
            <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center',  paddingBottom:10 }}>
                <Carousel
                  layout={"default"}
                  ref={isCarrousel}
                  data={rostizados}
                  sliderWidth={300}
                  itemWidth={300}
                  renderItem={_renderItem} />
            </View>
          </SafeAreaView>
        );
    
}

