import React from 'react'
import { View, Text,Image,SafeAreaView,TouchableHighlight } from 'react-native'
import { API_URL_IMAGES } from '../../utils/constants';
import { useNavigation } from "@react-navigation/native";
import { Button } from "react-native-paper";


export default function CarouselItem ({item}) {
    

    function goToProduct(idproduct){
        //const navigation = useNavigation();
        //navigation.push("producto", { idproduct: idproduct });
       alert(idproduct)
    }

    return (
        <SafeAreaView>
            <TouchableHighlight underlayColor='rgba(73,182,77,1,0.9)'
                key={item.idproduct}
              >
            <View style={{
            backgroundColor:'#f1f1f1',
            borderRadius: 20,
            height: 250,
            padding: 15,
            marginLeft:15,
            marginBottom:15,
            shadowColor: "black",
            shadowOffset: {
                width: 0,
                height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 10,
            }} >
                <Image  style={{ width: "100%", height: "70%"}}
                   source={{
                    uri: `${API_URL_IMAGES}${item.imagen}`,
                }}/>
                <Text style={{fontSize: 15}} >{item.producto}</Text>
                <Text style={{fontSize: 14, color:'#ff6900'}}>$ {item.precio}</Text>
                
                <Button
                    style={{                   
                    width: "100%",                  
                    borderRadius: 15,
                    borderWidth: 1,
                    backgroundColor: "#ff6900",
                    }}
                    icon="import"
                    mode="contained"
                    uppercase={false}
                    onPress={() => goToProduct(item.producto)}
                >
        <Text style={{ color: "white" }}> Ver detalle</Text>
        </Button>
        </View>
        </TouchableHighlight >
        </SafeAreaView>
        
    )
}
 