import React, { useState, useCallback } from "react";
import { ScrollView } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import Menu from "../../components/Auth/Account/Menu";
import {getUserApi} from "../../api/user";
import useAuth from "../../hooks/useAuth";
import { useIsFocused } from '@react-navigation/native';

export default function Account() {
  const [user, setUser] = useState(null);
  const { auth } = useAuth();

  const isFocused = useIsFocused();

  useFocusEffect(
    useCallback(() => {
      (async () => {
        const response = await getUserApi(auth.userid);
         setUser(response);
      })();
    }, [isFocused])
  );

  return (
    <>
     
         
          <ScrollView>
           
            <Menu />
          </ScrollView>
        </>
    
  );
}